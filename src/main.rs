use anyhow::{Result, Context};
use image::{RgbImage, Rgb};
use indicatif::ProgressIterator;

fn scale(min_in: i32, max_in: i32, min_out: f64, max_out: f64, value_in: i32) -> f64 {
    min_out + (((value_in - min_in) as f64) / ((max_in - min_in) as f64)) * (max_out - min_out)
}

fn main() -> Result<()> {
    const IMAGE_SIZE: u32 = 500;
    const X_RANGE: (f64, f64) = (-1.0, 1.0);
    const Y_RANGE: (f64, f64) = (-1.0, 1.0);

    let mut img = RgbImage::new(IMAGE_SIZE, IMAGE_SIZE);

    for px in (0..IMAGE_SIZE).progress() {
        for py in 0..IMAGE_SIZE {
            let x0 = scale(0, IMAGE_SIZE as i32, X_RANGE.0, X_RANGE.1, px as i32);
            let y0 = scale(0, IMAGE_SIZE as i32, Y_RANGE.0, Y_RANGE.1, py as i32);
            let mut x = 0.0;
            let mut y = 0.0;
            let mut iteration = 0;
            const MAX_ITERATIONS: usize = 1000;
            while (x * x + y * y <= 2.0 * 2.0) && (iteration < MAX_ITERATIONS) {
                let newx = x * x - y * y + x0;
                y = 2.0 * x * y + y0;
                x = newx;
                iteration += 1;
            }
            let pix_value = ((iteration / MAX_ITERATIONS) * 255) as u8;
            img.put_pixel(px, py, Rgb([pix_value, 0, 0]));
        }
    }
    img.save("img.png").context("unable to save image")?;
    Ok(())
}
